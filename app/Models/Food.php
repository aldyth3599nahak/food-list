<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Food extends Model
{
    protected $table = 'food';
    use SoftDeletes;
    protected $fillable = ['id','name','description','category','price','image'];
    protected $hidden = ['created_at','updated_ad','deleted_at'];
}

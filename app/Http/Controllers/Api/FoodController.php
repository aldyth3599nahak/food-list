<?php

namespace App\Http\Controllers\Api;
use App\Models\Food;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FoodController extends Controller
{
        //API function start
        public function index(Request $request)
        {
            $menu = Food::get();
            return response()->json($menu);//return data from database as json format
        }
    
        public function store(Request $request)
        {
            $request->validate([
                'name' => 'required',
                'price' => 'required',
                'category' => 'required'
            ]);
            $menu = new Food();
            $menu->name = $request->name;
            $menu->price = $request->price;
            $menu->category = $request->category;
            $menu->description = isset($request->description) ? $request->description:'';
            $menu->image = isset($request->image) ? $request->image:'';
            $menu->save();
            return response()->json([
                "success" => true,
                "message" => "successfully inserted data"
            ]);
        }
    
        public function update(Request $request, Food $Food, $id)
        {
            $id = $request->input('id');
            $request->validate([
                'name' => 'required',
                'price' => 'required',
                'category' => 'required'
            ]);
            $food = Food::where('id', $id);
            if(!$food){
                return response()->json([
                    "success" => false,
                    "message" => "id not found"
                ]);
            }
            $food->update($request->except(['_token']));
            return response()->json([
                "success" => true,
                "message" => "successfully updated data"
            ]);
        }
    
        public function destroy(Food $Food, $id)
        { //Delete data by id
            $food = Food::where('id', $id);//->delete();
            if(!$food){
                return response()->json([
                    "success" => false,
                    "message" => "id not found"
                ]);
            }
            $food->delete();
            return response()->json([
                "success" => true,
                "message" => "successfully deleted data"
            ]);
        }
        //API function end
}

<?php

namespace App\Http\Controllers;
use App\Models\Food;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() 
    { //parsing data into index.blade.php
        //Use 3 where function because of the UI desain 
        $menu = Food::where('category','mainDish')->get();
        $menu1 = Food::where('category','drinks')->get();
        $menu2 = Food::where('category','dessert')->get();
        return view('/menus/index', compact('menu','menu1','menu2'));
    }

    public function menu()
    { //parsing data into menus/edit.blade.php
        $menu = Food::where('category','mainDish')->get();
        $menu1 = Food::where('category','drinks')->get();
        $menu2 = Food::where('category','dessert')->get();
        return view('/menus/edit', compact('menu','menu1','menu2'));
    }
    
    public function cart()
    { //return view menus/cart.blade.php
        return view('/menus/cart', compact('menu','menu1','menu2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { //add new data to database
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'category' => 'required'
        ]);
        $menu = new Food();
        $menu->name = $request->name;
        $menu->price = $request->price;
        $menu->category = $request->category;
        $menu->description = isset($request->description) ? $request->description:'';
        $menu->image = isset($request->image) ? $request->image:'';
        $menu->save();
        // Food::create($request->all());
        return redirect('/menus/edit')->with('success','menu created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Food  $Food
     * @return \Illuminate\Http\Response
     */
    public function show(Food $Food)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Food  $Food
     * @return \Illuminate\Http\Response
     */
    public function edit(Food $Food)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Food  $Food
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Food $Food)
    { //update start
        $id = $request->input('id');
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'category' => 'required'
        ]);
        $food = Food::where('id', $id);
        if(!$food){
            return redirect('/menus/edit')->with('error','id not found');
        }
        $food->update($request->except(['_token']));
        return redirect('/menus/edit')->with('success','menu created successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Food  $Food
     * @return \Illuminate\Http\Response
     */
    public function destroy(Food $Food, $id)
    { //Delete menu by id
        $food = Food::where('id', $id);//->delete();
        if(!$food){
            return redirect('/menus/edit')->with('error','id not found');
        }
        $food->delete();
        return redirect('/menus/edit')->with('success','menu deleted successfully');
    }
}
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Food</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">

    <link rel="stylesheet" href="{{URL::asset('css/open-iconic-bootstrap.min.css')}}')}}">
    <link rel="stylesheet" href="{{URL::asset('css/animate.css')}}">
    
    <link rel="stylesheet" href="{{URL::asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{URL::asset('css/aos.css')}}">

    <link rel="stylesheet" href="{{URL::asset('css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{URL::asset('css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/jquery.timepicker.css')}}">

    
    <link rel="stylesheet" href="{{URL::asset('css/flaticon.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/icomoon.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
  </head>
  <body>
  	<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="index.html">Coffee<small>Blend</small></a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>
	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
	          <li class="nav-item active"><a href="{{url('/')}}" class="nav-link">Menu</a></li>
	          <li class="nav-item"><a href="{{route('menus.edit')}}" class="nav-link">Edit Menu</a></li>
	          <li class="nav-item cart"><a href="{{url('menus.cart')}}" class="nav-link"><span class="icon icon-shopping_cart"></span><span class="bag d-flex justify-content-center align-items-center"><small>1</small></span></a></li>
	        </ul>
	      </div>
		  </div>
	  </nav>
    <!-- END nav -->

    <section class="home-slider owl-carousel">

      <div class="slider-item" style="background-image: url(images/bg_3.jpg);" data-stellar-background-ratio="0.5">
      	<div class="overlay"></div>
        <div class="container">
          <div class="row slider-text justify-content-center align-items-center">

            <div class="col-md-7 col-sm-12 text-center ftco-animate">
            	<h1 class="mb-3 mt-5 bread">Edit Menu</h1>
            </div>

          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ftco-cart">
        <div class="container">
            <div class="row">
            <div class="col-md-12 ftco-animate">
                <div class="cart-list">
                    <table class="table">
                        <thead class="thead-primary">
                          <tr class="text-center">
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="text-center">
                            <td class="product-remove"><a href="#"><span class="icon-close"></span></a></td>
                            
                            <td class="image-prod"><div class="img" style="background-image:url(images/menu-2.jpg);"></div></td>
                            
                            <td class="product-name">
                                <h3>Creamy Latte Coffee</h3>
                                <p>Far far away, behind the word mountains, far from the countries</p>
                            </td>
                            
                            <td class="price">$4.90</td>
                            
                            <td class="quantity">
                                <div class="input-group mb-3">
                                 <input type="text" name="quantity" class="quantity form-control input-number" value="1" min="1" max="100">
                              </div>
                          </td>
                            
                            <td class="total">$4.90</td>
                          </tr><!-- END TR-->

                          <tr class="text-center">
                            <td class="product-remove"><a href="#"><span class="icon-close"></span></a></td>
                            
                            <td class="image-prod"><div class="img" style="background-image:url(images/dish-2.jpg);"></div></td>
                            
                            <td class="product-name">
                                <h3>Grilled Ribs Beef</h3>
                                <p>Far far away, behind the word mountains, far from the countries</p>
                            </td>
                            
                            <td class="price">$15.70</td>
                            
                            <td class="quantity">
                                <div class="input-group mb-3">
                                 <input type="text" name="quantity" class="quantity form-control input-number" value="1" min="1" max="100">
                              </div>
                          </td>
                            
                            <td class="total">$15.70</td>
                          </tr><!-- END TR-->
                        </tbody>
                      </table>
                  </div>
            </div>
        </div>
        <div class="row justify-content-end">
            <div class="col col-lg-3 col-md-6 mt-5 cart-wrap ftco-animate">
                <div class="cart-total mb-3">
                    <h3>Cart Totals</h3>
                    <p class="d-flex">
                        <span>Subtotal</span>
                        <span>$20.60</span>
                    </p>
                    <p class="d-flex">
                        <span>Delivery</span>
                        <span>$0.00</span>
                    </p>
                    <p class="d-flex">
                        <span>Discount</span>
                        <span>$3.00</span>
                    </p>
                    <hr>
                    <p class="d-flex total-price">
                        <span>Total</span>
                        <span>$17.60</span>
                    </p>
                </div>
                <p class="text-center"><a href="checkout.html" class="btn btn-primary py-3 px-4">Proceed to Checkout</a></p>
            </div>
        </div>
        </div>
    </section>

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="{{URL::asset('js/jquery.min.js')}}"></script>
  <script src="{{URL::asset('js/jquery-migrate-3.0.1.min.js')}}"></script>
  <script src="{{URL::asset('js/popper.min.js')}}"></script>
  <script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
  <script src="{{URL::asset('js/jquery.easing.1.3.js')}}"></script>
  <script src="{{URL::asset('js/jquery.waypoints.min.js')}}"></script>
  <script src="{{URL::asset('js/jquery.stellar.min.js')}}"></script>
  <script src="{{URL::asset('js/owl.carousel.min.js')}}"></script>
  <script src="{{URL::asset('js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{URL::asset('js/aos.js')}}"></script>
  <script src="{{URL::asset('js/jquery.animateNumber.min.js')}}"></script>
  <script src="{{URL::asset('js/bootstrap-datepicker.js')}}"></script>
  <script src="{{URL::asset('js/jquery.timepicker.min.js')}}"></script>
  <script src="{{URL::asset('js/scrollax.min.js')}}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="{{URL::asset('js/google-map.js')}}"></script>
  <script src="{{URL::asset('js/main.js')}}"></script>

  <script>

	$('#edit').on('show.bs.modal', function (event) {
  
	var button = $(event.relatedTarget)
	var id = button.data('id')
	var name = button.data('name')
	var category = button.data('category')
	var description = button.data('description')
	var price = button.data('price')
	var modal = $(this)
  
	modal.find('.modal-body #Id').val(id);
	modal.find('.modal-body #Name').val(name);
	modal.find('.modal-body #Category').val(category);
	modal.find('.modal-body #Description').val(description);
	modal.find('.modal-body #Price').val(price);
  })
  </script>
  </body>
</html>
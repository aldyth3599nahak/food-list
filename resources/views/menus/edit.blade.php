<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Food</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">

    <link rel="stylesheet" href="{{URL::asset('css/open-iconic-bootstrap.min.css')}}')}}">
    <link rel="stylesheet" href="{{URL::asset('css/animate.css')}}">
    
    <link rel="stylesheet" href="{{URL::asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{URL::asset('css/aos.css')}}">

    <link rel="stylesheet" href="{{URL::asset('css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{URL::asset('css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/jquery.timepicker.css')}}">

    
    <link rel="stylesheet" href="{{URL::asset('css/flaticon.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/icomoon.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
  </head>
  <body>
  	<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="index.html">Coffee<small>Blend</small></a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>
	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
	          <li class="nav-item active"><a href="{{url('/')}}" class="nav-link">Menu</a></li>
	          <li class="nav-item"><a href="{{route('menus.edit')}}" class="nav-link">Edit Menu</a></li>
	          <li class="nav-item cart"><a href="{{route('menus.cart')}}" class="nav-link"><span class="icon icon-shopping_cart"></span><span class="bag d-flex justify-content-center align-items-center"><small>1</small></span></a></li>
	        </ul>
	      </div>
		  </div>
	  </nav>
    <!-- END nav -->

    <section class="home-slider owl-carousel">

      <div class="slider-item" style="background-image: url(images/bg_3.jpg);" data-stellar-background-ratio="0.5">
      	<div class="overlay"></div>
        <div class="container">
          <div class="row slider-text justify-content-center align-items-center">

            <div class="col-md-7 col-sm-12 text-center ftco-animate">
            	<h1 class="mb-3 mt-5 bread">Edit Menu</h1>
            </div>

          </div>
        </div>
      </div>
    </section>

    <section class="ftco-intro">
    	<div class="container-wrap">
    		<div class="wrap d-md-flex align-items-xl-end">
    			<div class="info">
	    		</div>
	    		<div class="book p-4">
	    			<h3>add new Menu</h3>
	    			<form method="POST" class="appointment-form" action="{{route('menus.create')}}" enctype="multipart/form-data">
					@csrf
	    				<div class="d-md-flex">
							<input type="hidden" class="form-control" id="id" name="id">
		    				<div class="form-group">
								
		    					<input type="text" class="form-control" id="name" name="name" placeholder="name">
		    				</div>
		    				<div class="form-group ml-md-4">
							<div class="select-wrap">
							<div class="icon"><span class="ion-ios-arrow-down"></span></div>
		    				<select id="category" name="category" class="form-control">
		    					<option value="mainDish">Main Dish</option>
		    					<option value="drinks">Drinks</option>
		    					<option value="dessert">Dessert</option>
							</select>
							</div>
		    				</div>
	    				</div>
	    				<div class="d-md-flex">
		    				<div class="form-group">
		    					<input type="text" class="form-control" id="price" name="price" placeholder="price">
		    				</div>
		    				<div class="form-group ml-md-4">
		    					<input type="file" class="form-control" id="image" name="image" placeholder="image">
		    				</div>
	    				</div>
	    				<div class="d-md-flex">
	    				<div class="form-group text-wrap">
		              <textarea name="description" id="description" cols="30" rows="2" class="form-control" name="description" placeholder="description"></textarea>
		            </div>
		            <div class="form-group ml-md-4">
		              <button type="submit" class="btn btn-white py-3 px-4">add to menu</button>
		            </div>
	    				</div>
	    			</form>
	    		</div>
    		</div>
    	</div>
    </section>

 	<section class="ftco-section">
    	<div class="container">
        <div class="row">
        	<div class="col-md-6 mb-5 pb-3">
				<h3 class="mb-5 heading-pricing ftco-animate">Main Dish</h3>
				@foreach ($menu as $menu)
        		<div class="pricing-entry d-flex ftco-animate">
        			<div class="img" style="background-image: url(images/{{$menu->image}});"></div>
        			<div class="desc pl-3">
	        			<div class="d-flex text align-items-center">
	        				<h3><span>{{$menu->name}}</span></h3>
	        				<span class="price">${{$menu->price}}</span>
	        			</div>
	        			<div class="d-block">
	        				<p>{{$menu->description}}</p>
	        			</div>
					</div>
					<div class="footer">
						<button class="btn btn-white" data-name="{{$menu->name}}" data-id="{{$menu->id}}" 
							data-description="{{$menu->description}}" data-price="{{$menu->price}}" 
							data-category="{{$menu->category}}" data-toggle="modal"data-target="#edit">edit</button>
						{{--  <a href="{{route('menus.delete',[$menu->id])}}" class="btn btn-white" onclick="return confirm('delete this menu?')">delete</a>  --}}
						<form method="post" action="{{route('menus.delete',$menu->id)}}"> 
							@method('DELETE')
							@csrf
							<button class="btn btn-white" onclick="return confirm('delete this menu?')">delete</button>
						</form>
					</div>
        		</div>
				@endforeach
			</div>
			
			<div class="col-md-6 mb-5 pb-3">
				<h3 class="mb-5 heading-pricing ftco-animate">Drinks</h3>
				@foreach ($menu1 as $menu1)
        		<div class="pricing-entry d-flex ftco-animate">
        			<div class="img" style="background-image: url(images/{{$menu1->image}});"></div>
        			<div class="desc pl-3">
	        			<div class="d-flex text align-items-center">
	        				<h3><span>{{$menu1->name}}</span></h3>
	        				<span class="price">${{$menu1->price}}</span>
	        			</div>
	        			<div class="d-block">
	        				<p>{{$menu1->description}}</p>
	        			</div>
					</div>
					<div class="footer">
						<button class="btn btn-white" data-name="{{$menu1->name}}" data-id="{{$menu1->id}}" 
							data-description="{{$menu1->description}}" data-price="{{$menu1->price}}" 
							data-category="{{$menu1->category}}" data-toggle="modal"data-target="#edit">edit</button>
						{{--  <a href="{{route('menus.delete',$menu1->id)}}" data-method="delete" class="btn btn-white" onclick="return confirm('delete this menu?')">delete</a>  --}}
						<form method="post" action="{{route('menus.delete',$menu->id)}}"> 
							@method('DELETE')
							@csrf
							<button class="btn btn-white" onclick="return confirm('delete this menu?')">delete</button>
						</form>	
					</div>
        		</div>
				@endforeach
			</div>

			<div class="col-md-6 mb-5 pb-3">
				<h3 class="mb-5 heading-pricing ftco-animate">Dessert</h3>
				@foreach ($menu2 as $menu2)
        		<div class="pricing-entry d-flex ftco-animate">
        			<div class="img" style="background-image: url(images/{{$menu1->image}});"></div>
        			<div class="desc pl-3">
	        			<div class="d-flex text align-items-center">
	        				<h3><span>{{$menu2->name}}</span></h3>
	        				<span class="price">${{$menu2->price}}</span>
	        			</div>
	        			<div class="d-block">
	        				<p>{{$menu2->description}}</p>
	        			</div>
					</div>
					<div class="footer">
						<button class="btn btn-white" data-name="{{$menu2->name}}" data-id="{{$menu2->id}}" 
							data-description="{{$menu2->description}}" data-price="{{$menu2->price}}" 
							data-category="{{$menu2->category}}" data-toggle="modal"data-target="#edit">edit</button>
						{{--  <a href="{{route('menus.delete',$menu2->id)}}" data-method="delete" class="btn btn-white" onclick="return confirm('delete this menu?')">delete</a>  --}}
						<form method="post" action="{{route('menus.delete',$menu->id)}}"> 
							@method('DELETE')
							@csrf
							<button class="btn btn-white" onclick="return confirm('delete this menu?')">delete</button>
						</form>
					</div>
        		</div>
				@endforeach
        	</div>
        </div>
    	</div>
    </section>
	 
	
    <!-- Modal edit-->
   <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="id">Edit Menu</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="POST" action="{{route('menus.update')}}" enctype="multipart/form-data">
		@csrf
		<div class="d-md-flex">
			<input type="hidden" class="form-control" id="id" name="id">
			<div class="form-group">
				<label>Name</label>
				<input type="text" class="form-control" id="name" name="name" placeholder="name">
				<label>Category</label>
			<select id="category" name="category" class="form-control">
				<option value="mainDish">Main Dish</option>
				<option value="Drinks">Drinks</option>
				<option value="Dessert">Dessert</option>
			</select>
			<label>Price</label>
				<input type="text" class="form-control" id="price" name="price" placeholder="price">
			<label>Image</label>
				<input type="file" class="form-control" id="image" name="image" placeholder="image">
			<label>Description</label>  
				<textarea name="description" id="description" cols="30" rows="2" class="form-control" name="description" placeholder="description"></textarea>
	  		<button type="submit" class="btn btn-white py-3 px-4">update menu</button>
			</div>
		</div>
      </form>

      </div>
      <div class="modal-footer">
 
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- end modal -->

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="{{URL::asset('js/jquery.min.js')}}"></script>
  <script src="{{URL::asset('js/jquery-migrate-3.0.1.min.js')}}"></script>
  <script src="{{URL::asset('js/popper.min.js')}}"></script>
  <script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
  <script src="{{URL::asset('js/jquery.easing.1.3.js')}}"></script>
  <script src="{{URL::asset('js/jquery.waypoints.min.js')}}"></script>
  <script src="{{URL::asset('js/jquery.stellar.min.js')}}"></script>
  <script src="{{URL::asset('js/owl.carousel.min.js')}}"></script>
  <script src="{{URL::asset('js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{URL::asset('js/aos.js')}}"></script>
  <script src="{{URL::asset('js/jquery.animateNumber.min.js')}}"></script>
  <script src="{{URL::asset('js/bootstrap-datepicker.js')}}"></script>
  <script src="{{URL::asset('js/jquery.timepicker.min.js')}}"></script>
  <script src="{{URL::asset('js/scrollax.min.js')}}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="{{URL::asset('js/google-map.js')}}"></script>
  <script src="{{URL::asset('js/main.js')}}"></script>

  <script>

	$('#edit').on('show.bs.modal', function (event) {
  
	var button = $(event.relatedTarget)
	var id = button.data('id')
	var name = button.data('name')
	var category = button.data('category')
	var description = button.data('description')
	var price = button.data('price')
	var modal = $(this)
  
	modal.find('.modal-body #id').val(id);
	modal.find('.modal-body #name').val(name);
	modal.find('.modal-body #category').val(category);
	modal.find('.modal-body #description').val(description);
	modal.find('.modal-body #price').val(price);
  })
  </script>

	

  </body>
</html>
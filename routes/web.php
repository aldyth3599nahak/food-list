<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::name('/')->get('', 'FoodController@index');

Route::name('menus.')->prefix('menus')->group (function () {
    Route::name('edit')->get('edit', 'FoodController@menu');
    Route::name('cart')->get('cart', 'FoodController@cart');
    // Route::get('/', 'FoodController@index');
    // Route::get('/menu', 'FoodController@menu');
    // Route::get('/cart', 'FoodController@cart');
    Route::name('create')->post('store', 'FoodController@store');
    Route::name('update')->post('update', 'FoodController@update');
    Route::name('delete')->delete('delete/{id}', 'FoodController@destroy');

    // Route::post('create', 'FoodController@store');
    // Route::delete('delete/{id}', 'FoodController@destroy');
    // Route::post('update', 'FoodController@update');

});
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::name('')->prefix('')->namespace('Api')->group (function () {
    Route::name('index')->get('index', 'FoodController@index');
    Route::name('store')->post('store', 'FoodController@store');
    Route::name('delete')->delete('delete/{id}', 'FoodController@destroy');
    Route::name('update')->get('update/{id}', 'FoodController@update');

    // Route::get('/index', 'FoodController@index');
    // Route::post('/store', 'FoodController@store');
    // Route::delete('/deleteList/{id}', 'FoodController@destroy');
    // Route::post('/updateList/{id}', 'FoodController@update');
});